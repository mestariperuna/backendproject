using System;
using System.Threading.Tasks;
using backendproject.Exceptions;
using backendproject.Models;
using backendproject.Processors;
using Microsoft.AspNetCore.Mvc;

namespace backendproject.Controllers
{
    [Route("api/players")]
    public class PlayersController : Controller
    {
        private PlayersProcessor _processor;
        public PlayersController(PlayersProcessor processor)
        {
            _processor = processor;
        }

        [HttpGet("{name}")]
        public async Task<Player> GetPlayerByName(string name)
        {
            if(!string.IsNullOrEmpty(name))
                return await _processor.GetPlayerByName(name);
            throw new NotFoundException();
        }

        [HttpGet]
        public async Task<Player[]> GetPlayers()
        {
            return await _processor.GetPlayers();
        }

        [HttpPost]
        public async Task<Player> UpsertPlayer([FromBody]NewPlayer newPlayer)
        {
            return await _processor.UpsertPlayer(newPlayer);
        }

        [HttpDelete("{name}")]
        public async Task<Player> DeletePlayer(string name)
        {
            if(!string.IsNullOrEmpty(name))
                return await _processor.DeletePlayer(name);
            throw new NotFoundException();
        }

        [HttpGet("top10")]
        public async Task<Player[]> GetTop10PlayersByMapOrder(int? mapOrder, bool? score, bool? killCount, bool? lifeTime)
        {
            if(mapOrder != null)
            {
                if(score == true)
                    return await _processor.GetTop10PlayersByMapScore((int)mapOrder);
                
                if(killCount == true)
                    return await _processor.GetTop10PlayersByMapKillCount((int)mapOrder);

                if(lifeTime == true)
                    return await _processor.GetTop10PlayersByMapLifeTime((int)mapOrder);
        
            }
            throw new BadRequestException();
        }

        [HttpGet("avg")]
        public async Task<double> GetAverage(bool? score, bool? killCount, bool? lifeTime)
        {
            if(score == true)
                return await _processor.GetAverageScore();
            else if(killCount == true)
                return await _processor.GetAverageKillCount();
            else if(lifeTime == true)
                return await _processor.GetAverageLifeTime();
            else
                throw new BadRequestException();
        }

        [HttpGet("average")]
        public async Task<double[]> GetAverageMapData()
        {
            return await _processor.GetAverageMapData();
        }
    }
}