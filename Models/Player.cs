using System;
using System.Collections.Generic;

namespace backendproject.Models
{
    public class Player
    {
        public Guid Id {get;set;}
        public string Name {get;set;}
        public List<Map> Maps {get;set;}
    }
}