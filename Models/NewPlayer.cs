using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace backendproject.Models
{
    public class NewPlayer
    {
        [Required]
        public string Name {get;set;}
        public Map Map {get;set;}
    }
}