using System;
namespace backendproject.Models
{
    public class Map
    {
        public string MapName {get;set;}
        public int Score {get;set;}
        public int KillCount {get;set;}
        public float FavouriteWeaponTimer{get;set;}
        public string FavouriteWeapon {get;set;}
        public float LifeTime {get;set;}
    }
}