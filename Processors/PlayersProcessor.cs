using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using backendproject.Models;
using backendproject.Repositories;

namespace backendproject.Processors
{
    public class PlayersProcessor
    {
        private IRepository _repository;
        public PlayersProcessor(IRepository repository)
        {
            _repository = repository;
        }


        public async Task<Player> GetPlayerByName(string name)
        {
            return await _repository.GetPlayerByName(name.ToLower());
        }

        public async Task<Player[]> GetPlayers()
        {
            return await _repository.GetPlayers();
        }

        public async Task<Player> UpsertPlayer(NewPlayer newPlayer)
        {
            Player player = await _repository.GetPlayerByName(newPlayer.Name.ToLower());

            if(player == null)
            {
                player = new Player()
                {
                    Id = new Guid(),
                    Name = newPlayer.Name.ToLower(),
                    Maps = new List<Map>(3)
                };
                
                Map firstMap = new Map()
                {
                    MapName = "Sandbox",
                    FavouriteWeapon = ""
                };
                player.Maps.Add(firstMap);

                Map secondMap = new Map()
                {
                    MapName = "Storage",
                    FavouriteWeapon = ""
                };
                player.Maps.Add(secondMap);

                Map thirdMap = new Map()
                {
                    MapName = "Woods",
                    FavouriteWeapon = ""
                };
                player.Maps.Add(thirdMap);

                for(int i = 0; i < player.Maps.Count; i++)
                {
                    if(player.Maps[i].MapName.Equals(newPlayer.Map.MapName))
                        player.Maps[i] = newPlayer.Map;
                }
                
                await _repository.CreatePlayer(player);
            }
            else
            {
                List<Map> maps = player.Maps;
                Map newMap = newPlayer.Map;

                for(int i = 0; i < maps.Count; i++)
                {
                    if(maps[i].MapName.Equals(newMap.MapName))
                    {
                        if(maps[i].FavouriteWeaponTimer < newMap.FavouriteWeaponTimer)
                        {
                            maps[i].FavouriteWeapon = newMap.FavouriteWeapon;
                            maps[i].FavouriteWeaponTimer = newMap.FavouriteWeaponTimer;
                        }
                            
                        if(maps[i].KillCount < newMap.KillCount)
                            maps[i].KillCount = newMap.KillCount;

                        if(maps[i].LifeTime < newMap.LifeTime)
                            maps[i].LifeTime = newMap.LifeTime;

                        if(maps[i].Score < newMap.Score)
                            maps[i].Score = newMap.Score;

                        break;
                    }
                }

                player.Maps = maps;
                await _repository.ModifyPlayer(player);

            }

            return player;
        }

        public async Task<Player> DeletePlayer(string name)
        {
            return await _repository.DeletePlayer(name.ToLower());
        }

        public async Task<Player[]> GetTop10PlayersByMapScore(int mapOrder)
        {
            return await _repository.GetTop10PlayersByMapScore(mapOrder);
        }

        public async Task<Player[]> GetTop10PlayersByMapKillCount(int mapOrder)
        {
            return await _repository.GetTop10PlayersByMapKillCount(mapOrder);
        }

        public async Task<Player[]> GetTop10PlayersByMapLifeTime(int mapOrder)
        {
            return await _repository.GetTop10PlayersByMapLifeTime(mapOrder);
        }

        public async Task<double[]> GetAverageMapData()
        {
            return await _repository.GetAverageMapData();
        }

        public async Task<double> GetAverageScore()
        {
            return await _repository.GetAverageScore();
        }

        public async Task<double> GetAverageKillCount()
        {
            return await _repository.GetAverageKillCount();
        }

        public async Task<double> GetAverageLifeTime()
        {
            return await _repository.GetAverageLifeTime();
        }
    }
}