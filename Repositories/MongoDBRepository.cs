using System.Linq;
using System.Threading.Tasks;
using backendproject.Models;
using backendproject.MongoDB;
using MongoDB.Bson;
using MongoDB.Driver;

namespace backendproject.Repositories
{
    public class MongoDBRepository : IRepository
    {
        private IMongoCollection<Player> _collection;
        private IMongoDatabase _database;

        public MongoDBRepository(MongoDBClient client)
        {
            _database = client.GetDataBase("shooter");
            _collection = _database.GetCollection<Player>("players");
        }

        public async Task<Player> GetPlayerByName(string name)
        {
            var filter = Builders<Player>.Filter.Eq(p => p.Name, name);
            var cursor = await _collection.FindAsync(filter);       
            var list = cursor.ToList();

            if(list.Count == 0)
                return null;
            else
                return list[0];
        }

        public async Task<Player[]> GetPlayers()
        {
            var list = await _collection.Find(_ => true).ToListAsync();
            return list.ToArray();
        }

        public async Task<Player> CreatePlayer(Player player)
        {
            await _collection.InsertOneAsync(player);
            return player;
        }

        public async Task<Player> ModifyPlayer(Player player)
        {
            var filter = Builders<Player>.Filter.Eq(p => p.Name, player.Name);
            await _collection.ReplaceOneAsync(filter, player);
            return player;
        }

        public async Task<Player> DeletePlayer(string name)
        {
            var filter = Builders<Player>.Filter.Eq(p => p.Name, name);
            var cursor = await _collection.FindAsync(filter);
            var player = await cursor.FirstAsync();

            await _collection.DeleteOneAsync(filter);
            return player;
        }

        public async Task<Player[]> GetTop10PlayersByMapScore(int mapOrder)
        {
           var filter = Builders<Player>.Filter.Empty;
           var sort = Builders<Player>.Sort.Descending(p => p.Maps[mapOrder].Score);

           var list = await _collection.Find(filter).Sort(sort).Limit(10).ToListAsync();
           return list.ToArray();
        }

        public async Task<Player[]> GetTop10PlayersByMapKillCount(int mapOrder)
        {
           var filter = Builders<Player>.Filter.Empty;
           var sort = Builders<Player>.Sort.Descending(p => p.Maps[mapOrder].KillCount);

           var list = await _collection.Find(filter).Sort(sort).Limit(10).ToListAsync();
           return list.ToArray();
        }

        public async Task<Player[]> GetTop10PlayersByMapLifeTime(int mapOrder)
        {
           var filter = Builders<Player>.Filter.Empty;
           var sort = Builders<Player>.Sort.Descending(p => p.Maps[mapOrder].LifeTime);

           var list = await _collection.Find(filter).Sort(sort).Limit(10).ToListAsync();
           return list.ToArray();
        }

        public async Task<double[]> GetAverageMapData()
        {
            var collection = _database.GetCollection<BsonDocument>("players");
            var aggregation = collection.Aggregate().Unwind(p => p["Maps"])
            .Group(new BsonDocument { { "_id", new BsonDocument ("id", BsonNull.Value) }, 
            { "avgScore", new BsonDocument("$avg", "$Maps.Score") },
            { "avgKillCount", new BsonDocument("$avg", "$Maps.KillCount") },
            { "avgLifeTime", new BsonDocument("$avg", "$Maps.LifeTime") } });
            
            var result = await aggregation.FirstAsync();
            double[] avgData = { result["avgScore"].AsDouble, result["avgKillCount"].AsDouble, result["avgLifeTime"].AsDouble };
            return avgData;
        }

        public async Task<double> GetAverageScore()
        {
            var collection = _database.GetCollection<BsonDocument>("players");
            var aggregation = collection.Aggregate().Unwind(p => p["Maps"])
            .Group(new BsonDocument { { "_id", new BsonDocument ("id", BsonNull.Value) }, 
            { "avg", new BsonDocument("$avg", "$Maps.Score") } });
            
            var result = await aggregation.FirstAsync();
            return result["avg"].AsDouble;
        }

        public async Task<double> GetAverageKillCount()
        {
            var collection = _database.GetCollection<BsonDocument>("players");
            var aggregation = collection.Aggregate().Unwind(p => p["Maps"])
            .Group(new BsonDocument { { "_id", new BsonDocument ("id", BsonNull.Value) }, 
            { "avg", new BsonDocument("$avg", "$Maps.KillCount") } });
            
            var result = await aggregation.FirstAsync();
            return result["avg"].AsDouble;
        }

        public async Task<double> GetAverageLifeTime()
        {
            var collection = _database.GetCollection<BsonDocument>("players");
            var aggregation = collection.Aggregate().Unwind(p => p["Maps"])
            .Group(new BsonDocument { { "_id", new BsonDocument ("id", BsonNull.Value) }, 
            { "avg", new BsonDocument("$avg", "$Maps.LifeTime") } });
            
            var result = await aggregation.FirstAsync();
            return result["avg"].AsDouble;
        }
    }
}