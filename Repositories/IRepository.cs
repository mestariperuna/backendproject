using System;
using System.Threading.Tasks;
using backendproject.Models;

namespace backendproject.Repositories
{
    public interface IRepository
    {
        Task<Player> GetPlayerByName(string name);
        Task<Player[]> GetPlayers();
        Task<Player> CreatePlayer(Player player);
        Task<Player> ModifyPlayer(Player player);
        Task<Player> DeletePlayer(string name);

        Task<Player[]> GetTop10PlayersByMapScore(int mapOrder);
        Task<Player[]> GetTop10PlayersByMapKillCount(int mapOrder);
        Task<Player[]> GetTop10PlayersByMapLifeTime(int mapOrder);
        Task<double[]> GetAverageMapData();
        Task<double> GetAverageScore();
        Task<double> GetAverageKillCount();
        Task<double> GetAverageLifeTime();
        
    }
}