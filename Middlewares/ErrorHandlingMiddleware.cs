using System.Threading.Tasks;
using backendproject.Exceptions;
using Microsoft.AspNetCore.Http;

namespace backendproject.Middlewares
{
    public class ErrorHandlingMiddleware
    {
        private RequestDelegate _next;
        public ErrorHandlingMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            bool notFound = false;
            try
            {
                await _next(context);
            }
            catch(NotFoundException e)
            {
                context.Response.StatusCode = 404;
                notFound = true;
            }
            catch(BadRequestException e)
            {
                context.Response.StatusCode = 400;
            }

            if(notFound)
            {
                await context.Response.WriteAsync("Could not find a player");
            }
        }
    }
}