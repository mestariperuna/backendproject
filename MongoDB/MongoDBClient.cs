using MongoDB.Driver;

namespace backendproject.MongoDB
{
    public class MongoDBClient
    {
        private readonly MongoClient _mongoClient;

        public MongoDBClient()
        {
            _mongoClient = new MongoClient("mongodb://localhost:27017");
        }

        public IMongoDatabase GetDataBase(string name)
        {
            return _mongoClient.GetDatabase(name);
        }
    }
}